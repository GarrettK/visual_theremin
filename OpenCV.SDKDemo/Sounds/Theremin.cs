﻿using System;
using Android.Media;
using Java.Lang;
using Java.Util.Concurrent;
using Visual_Theremin.Struct;

namespace Visual_Theremin.Sound
{
    public class Theremin
    {
        private Semaphore varLock = new Semaphore(1);

        private int sampleLength = 64;
        private int freqRange = 11025;

        // 1 to maxTones independent tones
        private int maxTones = 6;
        private int howl = 2;

        private double warbMag = 25.0;
        private double warbFreq = 0.08;
        private double freq = 1000.0;
        private double volume = 0.5;

        private double warbMagMax = 25;
        private double warbFreqMax = 0.15;
        private double freqMax = 2000;
        private double volumeMax = 1;

        private bool go = true;
        private bool running = false;

        private AudioTrack Track;

        // To use percent enter a value between 0.0 - 1.0
        // magnitude of warble oscillation, suggest 0.0 to 50
        private double WarbleMagnitudePercent
        {
            get
            {
                return warbMag / warbMagMax;
            }
            set
            {
                double wm = warbMagMax * value;
                try
                {

                    varLock.Acquire();
                }
                catch (Java.Lang.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    warbMag = wm;
                    varLock.Release();
                }
            }
        }

        // suggest 0.0 to 0.15
        private double WarbleFrequencyPercent
        {
            get
            {
                return (warbFreq / warbFreqMax) + 0.2;
            }
            set
            {
                double wf = warbFreqMax * value;
                try
                {
                    varLock.Acquire();
                }
                catch (Java.Lang.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    warbFreq = wf;
                    varLock.Release();
                }
            }
        }

        // range in hertz, suggest 200 to 2000
        private double FrequencyPercent
        {
            get
            {
                return freq / freqMax;
            }
            set
            {
                double fr = ((freqMax - 200) * value) + 200;
                try
                {
                    varLock.Acquire();
                }
                catch (Java.Lang.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    freq = fr;
                    varLock.Release();
                }
            }
        }

        // volume is 0.0 to 1.0
        private double VolumePercent
        {
            get
            {
                return volume / volumeMax;
            }
            set
            {
                double vol = volumeMax * value;
                try
                {
                    varLock.Acquire();
                }
                catch (Java.Lang.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    volume = vol;
                }
            }
        }

        public void Stop()
        {
            try
            {
                varLock.Acquire();
            }
            catch (Java.Lang.Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                go = false;
                varLock.Release();
            }
        }

        internal void ProcessHandData(object sender, HandPose e)
        {
            double X = e.normalizedCenterPoint.X;
            double Y = e.normalizedCenterPoint.Y;

            WarbleMagnitudePercent = e.amountOfFingers / 5;
            FrequencyPercent = X;
            WarbleFrequencyPercent = Y;
        }

        public void Start()
        {
            if (!running)
            {
                running = true;
                Run();
            }
            try
            {
                varLock.Acquire();
            }
            catch (Java.Lang.Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                go = true;
                varLock.Release();
            }
        }

        private void Howl(int h)
        {
            try
            {
                varLock.Acquire();
            }
            catch (Java.Lang.Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                howl = h;
                varLock.Release();
            }
        }

        private void Run()
        {
            void worker()
            {
                Runnable();
            }

            new Thread(worker).Start();

        }

        private void Runnable()
        {
            short[] samples = new short[sampleLength];
            double[] freqBase = new double[maxTones];
            int[] freqTrack = new int[maxTones];
            int[] freqVector = new int[maxTones];
            double[] angle = new double[maxTones];
            double collectedAngle;
            double computedWarble = 0;

            Random gen = new Random();

            int minSize = AudioTrack.GetMinBufferSize(freqRange, ChannelOut.Mono, Encoding.Pcm16bit);
            Track = new AudioTrack(Stream.Music, freqRange, ChannelOut.Mono, Encoding.Pcm16bit, minSize,
                AudioTrackMode.Stream);
            Track.Play();

            while (true)
            {
                try
                {
                    varLock.Acquire();
                }
                catch (Java.Lang.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    if (go)
                    {
                        // compute the base sample frequency
                        freqBase[0] = freq + warbMag * Java.Lang.Math.Sin(computedWarble);

                        // append any additional harmonics
                        for (int i = 1; i < howl; i++)
                        {
                            freqVector[i] += gen.Next(1, 3);
                            if (Java.Lang.Math.Abs(freqVector[i]) > 6) freqVector[i] = (int)(freqVector[i] / 2);
                            freqTrack[i] += freqVector[i];
                            if (Java.Lang.Math.Abs(freqTrack[i]) > 100) freqVector[i] = -freqVector[i];
                            freqBase[i] = freqBase[0] + freqTrack[i];
                        }

                        // fill sample
                        for (int i = 0; i < sampleLength; i++)
                        {
                            collectedAngle = 0;

                            // compute base tone and then append any additional
                            for (int j = 0; j < howl; j++)
                            {
                                angle[j] += 2 * Java.Lang.Math.Pi * freqBase[j] / freqRange;
                                if (angle[j] > (2 * Java.Lang.Math.Pi))
                                    angle[j] -= (2 * Java.Lang.Math.Pi);

                                collectedAngle += Java.Lang.Math.Sin(angle[j]);
                            }

                            samples[i] = (short)((collectedAngle / howl) * Short.MaxValue * volume);
                        }

                        // advance warble
                        computedWarble += warbFreq;
                        if (computedWarble > (2 * Java.Lang.Math.Pi))
                        {
                            computedWarble -= (2 * Java.Lang.Math.Pi);
                        }

                        // shove the sample into the stream
                        Track.Write(samples, 0, sampleLength);
                    }
                    else
                    {
                        // wait when stopped
                        try
                        {
                            Thread.Sleep(100);
                        }
                        catch (InterruptedException e)
                        {
                            e.PrintStackTrace();
                        }
                    }
                    varLock.Release();
                }
            }
        }
    }
}
