﻿using Android.Runtime;
using Java.Lang;
using OpenCV.Core;
using OpenCV.ImgProc;
using System.Collections.Generic;

namespace Visual_Theremin.MixedProcessing
{
    class HandDetector
    {
        // Minimum contour area in percent for contours filtering
        private static double mMinContourArea = 0.1;
        private Mat mSpectrum = new Mat();
        private List<MatOfPoint> mContours = new List<MatOfPoint>();
        private List<List<Point>> convDefects = new List<List<Point>>();
        // Cache
        Mat mDilatedMask = new Mat();
        Mat mHierarchy = new Mat();

        public int GetNumberOfFingers()
        {
            return Math.Min(convDefects.Count + 1, 5);
        }

        public void SetMinContourArea(double area)
        {
            mMinContourArea = area;
        }

        public void Process(Mat rgbaImage)
        {

            IList<MatOfPoint> contours = new JavaList<MatOfPoint>();

            Imgproc.FindContours(rgbaImage, contours, mHierarchy, Imgproc.RetrExternal, Imgproc.ChainApproxSimple);

            // Find max contour area
            double maxArea = 0;

            foreach (var each in contours)
            {
                MatOfPoint wrapper = each;
                double area = Imgproc.ContourArea(wrapper);
                if (area > maxArea)
                    maxArea = area;
            }

            // Filter contours by area and resize to fit the original image size
            mContours.Clear();
            foreach (var each in contours)
            {
                MatOfPoint contour = each;
                if (Imgproc.ContourArea(contour) > mMinContourArea * maxArea)
                {
                    mContours.Add(contour);
                }
            }
        }

        public List<MatOfPoint> Contours { get { return mContours; } }

        public int CompareContours(MatOfPoint a, MatOfPoint b)
        {
            return (int)(Imgproc.ContourArea(a) - Imgproc.ContourArea(b));
        }

        public void CalcConvexityDefects()
        {
            int tolerance = 25;
            double angleTolerance = 95;
            double minAngleTolerance = 5;
            convDefects.Clear();
            List<MatOfInt4> convDef = new List<MatOfInt4>();

            for (int i = 0; i < 1 && i < mContours.Count; i++)
            {
                MatOfInt hull = new MatOfInt();
                Imgproc.ConvexHull(mContours[i], hull);
                convDef.Add(new MatOfInt4());
                Imgproc.ConvexityDefects(mContours[i], hull, convDef[i]);
                int[] cdList = convDef[i].ToArray();
                Point[] data = mContours[i].ToArray();

                for (int j = 0; j < cdList.Length; j = j + 4)
                {
                    if (DistanceBetweenPoints(data[cdList[j]], data[cdList[j + 2]]) > tolerance
                        && DistanceBetweenPoints(data[cdList[j + 1]], data[cdList[j + 2]]) > tolerance
                        && GetAngle(data[cdList[j]], data[cdList[j + 2]], data[cdList[j + 1]]) < angleTolerance
                        && GetAngle(data[cdList[j]], data[cdList[j + 2]], data[cdList[j + 1]]) > minAngleTolerance)
                    {
                        List<Point> defects = new List<Point>();
                        defects.Add(data[cdList[j]]); //start
                        defects.Add(data[cdList[j + 1]]); //end
                        defects.Add(data[cdList[j + 2]]); // far
                        convDefects.Add(defects);
                    }
                }
            }
            RejectConvexityDefects();
        }

        private double DistanceBetweenPoints(Point a, Point b)
        {
            return Math.Sqrt(Math.Abs(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2)));
        }

        private void RejectConvexityDefects()
        {
            int tolerance = 2;
            for (int i = 0; i < convDefects.Count; i++)
            {
                for (int j = i; j < convDefects.Count; j++)
                {
                    if (DistanceBetweenPoints(convDefects[i][0], convDefects[j][2]) < tolerance ||
                        DistanceBetweenPoints(convDefects[i][1], convDefects[j][2]) < tolerance)
                    {
                        convDefects.RemoveAt(j);
                        j--;
                    }
                    else if (DistanceBetweenPoints(convDefects[i][2], convDefects[j][0]) < tolerance ||
                        DistanceBetweenPoints(convDefects[i][2], convDefects[j][1]) < tolerance)
                    {
                        convDefects.RemoveAt(i);
                        i--;
                        break;
                    }
                }
            }

        }

        private double GetAngle(Point s, Point f, Point e)
        {
            double l1 = DistanceBetweenPoints(f, s);
            double l2 = DistanceBetweenPoints(f, e);
            double dot = (s.X - f.X) * (e.X - f.X) + (s.Y - f.Y) * (e.Y - f.Y);
            double angle = Math.Acos(dot / (l1 * l2));
            angle = angle * 180 / Math.Pi;
            return angle;
        }

        public void DrawConvexityDefects(Mat image)
        {
            int s = 0;
            int d = 0;
            int f = 0;
            foreach (var def in convDefects)
            {
                Imgproc.Circle(image, def[0], 5 + d++, new Scalar(255, 0, 0), 2);
                Imgproc.Circle(image, def[1], 5 + s++, new Scalar(255, 255, 0), 2);
                Imgproc.Circle(image, def[2], 5 + f++, new Scalar(0, 0, 255), 2);
            }
        }

        public Point CalcCenterofContour()
        {
            if (mContours.Count > 0)
            {
                Rect rect = Imgproc.BoundingRect(mContours[0]);
                int centerX = rect.X + rect.Width / 2;
                int centerY = rect.Y + rect.Height / 2;
                Point center = new Point(centerX, centerY);
                return center;
            }
            return null;
        }

    }
}