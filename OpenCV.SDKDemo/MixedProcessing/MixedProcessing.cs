using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using OpenCV.Core;
using OpenCV.Android;
using Android.Util;
using Java.Lang;
using OpenCV.ImgProc;
using Android.Content.PM;
using OpenCV.Video;
using Visual_Theremin.Sound;
using System;
using Visual_Theremin.Struct;

namespace Visual_Theremin.MixedProcessing
{
    [Activity(Label = "MixedProcessing", ScreenOrientation = ScreenOrientation.Landscape,
        ConfigurationChanges = ConfigChanges.KeyboardHidden | ConfigChanges.Orientation)]
    public class MixedProcessingActivity : Activity, CameraBridgeViewBase.ICvCameraViewListener2
    {
        public const string TAG = "OCVSample::Activity";

        public enum MODES
        {
            ACTIVE,
            INACTIVE,
        }

        private int mViewMode;
        private MODES currentMode = MODES.ACTIVE;
        private Mat frame;
        private Mat mIntermediateMat;
        private Mat mGray;
        private Mat fgmask;
        private BackgroundSubtractorMOG2 bsg;
        private HandDetector hDetect;
        private bool backgroundSet = false;
		private Theremin t;
		private EventHandler<HandPose> handProps;
        private OpenCV.Core.Size screenSize;

		public CameraBridgeViewBase mOpenCvCameraView { get; private set; }

        private BaseLoaderCallback mLoaderCallback;
        private Scalar CONTOUR_COLOR;

        public MixedProcessingActivity()
        {
            Log.Info(TAG, "Instantiated new " + GetType().ToString());
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {

            Log.Info(TAG, "called onCreate");
            base.OnCreate(savedInstanceState);
            mLoaderCallback = new Callback(this, this);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            SetContentView(Resource.Layout.tutorial2_surface_view);
            ActionBar.Hide();
            Window.DecorView.SystemUiVisibility = StatusBarVisibility.Hidden;

            screenSize = new OpenCV.Core.Size(720, 480);

            mOpenCvCameraView = FindViewById<CameraBridgeViewBase>(Resource.Id.tutorial2_activity_surface_view);
            mOpenCvCameraView.SetMaxFrameSize((int)screenSize.Width, (int)screenSize.Height);
            mOpenCvCameraView.SetCameraIndex(0); //set to front camera
            mOpenCvCameraView.Visibility = ViewStates.Visible;
            mOpenCvCameraView.SetCvCameraViewListener2(this);


			t = new Theremin();
			handProps += t.ProcessHandData;
            t.Start();
        }

		protected override void OnPause()
        {
            base.OnPause();
            if (mOpenCvCameraView != null)
                mOpenCvCameraView.DisableView();
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (!OpenCVLoader.InitDebug())
            {
                Log.Debug(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
                OpenCVLoader.InitAsync(OpenCVLoader.OpencvVersion300, this, mLoaderCallback);
            }
            else
            {
                Log.Debug(TAG, "OpenCV library found inside package. Using it!");
                mLoaderCallback.OnManagerConnected(LoaderCallbackInterface.Success);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (mOpenCvCameraView != null)
                mOpenCvCameraView.DisableView();
        }

        public void OnCameraViewStarted(int width, int height)
        {
            frame = new Mat(height, width, CvType.Cv8uc4);
            mIntermediateMat = new Mat(height, width, CvType.Cv8uc4);
            bsg = OpenCV.Video.Video.CreateBackgroundSubtractorMOG2();
            hDetect = new HandDetector();
            CONTOUR_COLOR = new Scalar(255, 0, 0, 255);
        }

        public void OnCameraViewStopped()
        {
            frame.Release();
            mIntermediateMat.Release();
        }

        public Mat OnCameraFrame(CameraBridgeViewBase.ICvCameraViewFrame inputFrame)
        {
            frame = inputFrame.Rgba();

            //OpenCV.Core.Core.Flip(frame, frame, 1);

            OpenCV.Core.Size sizeFrame = frame.Size();
            int rows = (int)sizeFrame.Height;
            int cols = (int)sizeFrame.Width;

            MODES mode = currentMode;
            switch (mode)
            {
                case MODES.INACTIVE:
                    break;
                case MODES.ACTIVE:

                    Imgproc.CvtColor(frame, frame, Imgproc.ColorRgba2rgb);
                    Imgproc.CvtColor(frame, frame, Imgproc.ColorRgb2hsv);

                    Scalar lowerThreshold = new Scalar(0, 48, 80); //lower color range for the hand 
                    Scalar upperThreshold = new Scalar(20, 255, 255); //upper range
                    Imgproc.Blur(frame, frame, new OpenCV.Core.Size(3, 3));

                    Mat tmp = new Mat();
                    Core.InRange(frame, lowerThreshold, upperThreshold, tmp);
					
                    frame.SetTo(new Scalar(0, 0, 0));
                    frame.SetTo(new Scalar(0, 0, 255), tmp);

                    int kSize = 11;
					Imgproc.Erode(frame, frame, Imgproc.GetStructuringElement(Imgproc.MorphRect, new OpenCV.Core.Size(kSize, kSize)));
					Imgproc.Erode(frame, frame, Imgproc.GetStructuringElement(Imgproc.MorphRect, new OpenCV.Core.Size(kSize, kSize)));
					Imgproc.Dilate(frame, frame, Imgproc.GetStructuringElement(Imgproc.MorphRect, new OpenCV.Core.Size(kSize, kSize)));
					Imgproc.Dilate(frame, frame, Imgproc.GetStructuringElement(Imgproc.MorphRect, new OpenCV.Core.Size(kSize, kSize)));

					Imgproc.CvtColor(frame, frame, Imgproc.ColorHsv2rgb);
					Imgproc.CvtColor(frame, frame, Imgproc.ColorRgb2gray);

					hDetect.Process(frame);
					List<MatOfPoint> contours = hDetect.Contours;

					Imgproc.CvtColor(frame, frame, Imgproc.ColorGray2rgba);

					Imgproc.DrawContours(frame, contours, 0, CONTOUR_COLOR);

					hDetect.CalcConvexityDefects();
					hDetect.DrawConvexityDefects(frame);
                    Point centerofHand = hDetect.CalcCenterofContour();

                    if(centerofHand != null)
                    {
                        Imgproc.PutText(frame, "X",
                            centerofHand,
                            OpenCV.Core.Core.FontHersheyComplex,
                            1,
                            new Scalar(255, 255, 255),
                            2);

                        HandPose h = new HandPose();
                        h.amountOfFingers = hDetect.GetNumberOfFingers();
                        Imgproc.PutText(frame, h.amountOfFingers.ToString(),
                            new Point(0, 25),
                            OpenCV.Core.Core.FontHersheyComplex,
                            1,
                            new Scalar(255, 255, 255),
                            2);
                        Point normalizedPoint = new Point(centerofHand.X / screenSize.Width, centerofHand.Y / screenSize.Height);
                        h.normalizedCenterPoint =  normalizedPoint;

                        handProps?.Invoke(this, h);
                    }
                    break;
            }
            return frame;
        }

        private void UpdateTheremin()
        {
            Point centerofHand = hDetect.CalcCenterofContour();
            int numberofFingers = hDetect.GetNumberOfFingers();
        }
    }

    class Callback : BaseLoaderCallback
    {
        private readonly MixedProcessingActivity _activity;
        public Callback(Context context, MixedProcessingActivity activity)
            : base(context)
        {
            _activity = activity;
        }
        public override void OnManagerConnected(int status)
        {
            switch (status)
            {
                case LoaderCallbackInterface.Success:
                    {
                        Log.Info(MixedProcessingActivity.TAG, "OpenCV loaded successfully");

                        // Load native library after(!) OpenCV initialization
                        JavaSystem.LoadLibrary("mixed_sample");

                        _activity.mOpenCvCameraView.EnableView();
                    }
                    break;
                default:
                    {
                        base.OnManagerConnected(status);
                    }
                    break;
            }
        }
    }
}