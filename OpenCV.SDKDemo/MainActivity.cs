﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Visual_Theremin.MixedProcessing;

namespace Visual_Theremin
{
    [Activity(Label = "Visual_Theremin", MainLauncher = true, Icon = "@drawable/icon"
		, ScreenOrientation = ScreenOrientation.Landscape)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
			
			StartActivity(typeof(MixedProcessingActivity));
        }
    }
}

