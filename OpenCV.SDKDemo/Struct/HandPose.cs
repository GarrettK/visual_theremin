﻿using OpenCV.Core;

namespace Visual_Theremin.Struct
{
	public class HandPose
	{
		public int amountOfFingers;
		public Point normalizedCenterPoint;
	}
}